﻿using System;
using System.Linq;

namespace GraphsLab1.Classes
{
    public class Graph
    {
        private readonly int[,] matrix;

        public Graph(int[,] matrix)
        {
            this.matrix = matrix;
        }

        public int[] GetEccentricities()
        {
            var size = matrix.GetLength(0);
            var matrixRoutes = GetRoutesMatrix();
            var result = new int[size];

            for (var i = 0; i < size; i++)
            {
                var extr = 0;

                for (var j = 0; j < size; j++)
                {
                    if (matrixRoutes[i, j] > extr)
                        extr = matrixRoutes[i, j];
                }

                result[i] = extr;
            }

            return result;
        }

        public int GetRadius()
        {
            var eccentricities = GetEccentricities();

            return eccentricities.Min();
        }

        public int GetDiameter()
        {
            var eccentricities = GetEccentricities();

            return eccentricities.Max();
        }

        public int[] GetVerticesDegreesVector()
        {
            var size = matrix.GetLength(0);
            var result = new int[size];

            for (var i = 0; i < size; i++)
            {
                var count = 0;

                for (var j = 0; j < size; j++)
                {
                    if (matrix[i, j] == 1)
                        count++;
                }

                result[i] = count;
            }

            return result;
        }

        public int[,] GetRoutesMatrix()
        {
            var size = matrix.GetLength(0);
            var A = new int[size, size];

            for (var i = 0; i < size; ++i)
            {
                for (var j = 0; j < size; ++j)
                {
                    if (matrix[i, j] == 0)
                        A[i, j] = 10000000;
                    else
                        A[i, j] = matrix[i, j];
                }
            }

            for (var k = 0; k < size; ++k)
            for (var i = 0; i < size; ++i)
            for (var j = 0; j < size; ++j)
                A[i, j] = Math.Min(A[i, j], A[i, k] + A[k, j]);

            return A;
        }
    }
}