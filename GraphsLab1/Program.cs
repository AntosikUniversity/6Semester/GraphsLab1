﻿using System;
using GraphsLab1.Classes;

namespace GraphsLab1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var matrix = new[,]
            {
                {1, 1, 0, 0, 1, 0},
                {1, 0, 1, 0, 1, 0},
                {0, 1, 0, 1, 0, 0},
                {0, 0, 1, 0, 1, 1},
                {1, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0}
            };

            var graph = new Graph(matrix);

            var radius = graph.GetRadius();
            var diameter = graph.GetDiameter();
            var degreesVector = graph.GetVerticesDegreesVector();
            var matrixRoutes = graph.GetRoutesMatrix();

            Console.WriteLine("Routes matrix: ");
            var size = matrix.GetLength(0);
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++) Console.Write($"{matrixRoutes[i, j]} ");
                Console.WriteLine();
            }
            Console.WriteLine();

            Console.WriteLine($"Radius: {radius}");
            Console.WriteLine($"Diameter: {diameter}");

            Console.Write("Degrees vector: ");
            foreach (var i in degreesVector) Console.Write($"{i} ");
            Console.WriteLine();
        }
    }
}